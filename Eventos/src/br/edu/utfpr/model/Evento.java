package br.edu.utfpr.model;

import java.time.LocalDate;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * 
 * @author talyssoncastro
 */
@Entity
public class Evento extends AbstractEntityImpl{
    
    private LongProperty idEvento = new SimpleLongProperty(this,"idEvento");
    private StringProperty descricao = new SimpleStringProperty(this, "descricao");
    private ObjectProperty<LocalDate> data = new SimpleObjectProperty<>(this, "data");
    private DoubleProperty valor = new SimpleDoubleProperty(this, "valor");
    private BooleanProperty possuiMeiaEntrada = new SimpleBooleanProperty(this, "possuiMeiaEntrada");
    private StringProperty local = new SimpleStringProperty(this, "local");
    private BooleanProperty permiteMenores = new SimpleBooleanProperty(this, "permiteMenores");
    private ObjectProperty<TipoTraje> tipoTraje = new SimpleObjectProperty<>(this, "tipoTraje");
    private IntegerProperty duracao = new SimpleIntegerProperty(this, "duracao");

    @Transient
    @Override
    public Long getId() {
        return idEvento.getValue();
    }

    @Override
    public void setId(Long id) {
        idEvento.setValue(id);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getIdEvento() {
        return idEvento.getValue();
    }

    public void setIdEvento(Long idEvento) {
        this.idEvento.setValue(idEvento);
    }

    public String getDescricao() {
        return descricao.get();
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Convert(converter = LocalDateConverter.class)
    @Column(name = "dataevento")
    public LocalDate getData() {
        return data.getValue();
    }

    public void setData(LocalDate data) {
        this.data.setValue(data);
    }

    public Double getValor() {
        return valor.getValue();
    }

    public void setValor(Double valor) {
        this.valor.setValue(valor);
    }

    public Boolean getPossuiMeiaEntrada() {
        return possuiMeiaEntrada.getValue();
    }

    public void setPossuiMeiaEntrada(Boolean possuiMeiaEntrada) {
        this.possuiMeiaEntrada.setValue(possuiMeiaEntrada);
    }

    @Column(name = "localevento")
    public String getLocal() {
        return local.getValue();
    }

    public void setLocal(String local) {
        this.local.setValue(local)  ;
    }

    public Boolean getPermiteMenores() {
        return permiteMenores.getValue();
    }

    public void setPermiteMenores(Boolean permiteMenores) {
        this.permiteMenores.setValue(permiteMenores);
    }

    public Integer getTipoTraje() {
        return tipoTraje.get() != null ? tipoTraje.get().toValue() : null;
    }

    public void setTipoTraje(Integer tipoTraje) {
        this.tipoTraje.setValue(TipoTraje.forValue(tipoTraje));
    }

    public Integer getDuracao() {
        return duracao.getValue();
    }

    public void setDuracao(Integer duracao) {
        this.duracao.setValue(duracao);
    }
    
    public LongProperty idEventoProperty(){
        return idEvento;
    }
    
    public StringProperty descricaoProperty(){
        return descricao;
    }
    
    public ObjectProperty<LocalDate> dataProperty(){
        return data;
    }
    
    public DoubleProperty valorProperty(){
        return valor;
    }
    
    public BooleanProperty possuiMeiaEntradaProperty(){
        return possuiMeiaEntrada;
    }
    
    public StringProperty localProperty(){
        return local;
    }
    
    public BooleanProperty permiteMenoresProperty(){
        return permiteMenores;
    }
    
    public ObjectProperty<TipoTraje> tipoTrajeProperty(){
        return tipoTraje;
    }
    
    public IntegerProperty duracaoProperty(){
        return duracao;
    }
    
         
}
