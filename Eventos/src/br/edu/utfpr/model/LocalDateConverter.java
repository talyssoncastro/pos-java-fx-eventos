/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.model;

import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * 
 * @author talyssoncastro
 */
@Converter(autoApply = true)
public class LocalDateConverter implements AttributeConverter<LocalDate, Date>{

    @Override
    public Date convertToDatabaseColumn(LocalDate attribute) {
        if (attribute != null){
            return Date.valueOf(attribute);
        }
        return null;
    }

    @Override
    public LocalDate convertToEntityAttribute(Date dbData) {
        if (dbData != null){
            return dbData.toLocalDate();
        }
        return null;
    }
    
}
