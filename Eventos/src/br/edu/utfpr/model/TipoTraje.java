/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.model;

/**
 *
 * @author talyssoncastro
 */
public enum TipoTraje {
    
    LIVRE(0, "Livre"), SOCIAL(1, "Social"), CAIPIRA(2, "Caipira"), HAWAI(3, "Hawai"), COWBOY(4, "Cowboy");
    
    private final Integer id;
    private final String descricao;

    TipoTraje(Integer id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public Integer getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }
    
    public static TipoTraje forValue(Integer id) {
        
        for (TipoTraje tipoTraje: values()) {
            if (tipoTraje.getId().equals(id)) {
                return tipoTraje;
            }
        }
        
        return LIVRE;
        
    }
    
    public Integer toValue() {
        return this.getId();
    }

}
