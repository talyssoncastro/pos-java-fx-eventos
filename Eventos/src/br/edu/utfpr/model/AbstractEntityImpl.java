package br.edu.utfpr.model;

/**
 * 
 * @author talyssoncastro
 */
public abstract class AbstractEntityImpl implements AbstractEntity {

    @Override
    public Boolean isPersistente() {
        return getId() != null && getId() != 0;
    }

}
