/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.model;

import java.io.Serializable;

/**
 * 
 * @author talyssoncastro
 */
public interface AbstractEntity extends Serializable {
    
    public Long getId();
    
    public void setId(Long id);
    
    public Boolean isPersistente(); 
}
