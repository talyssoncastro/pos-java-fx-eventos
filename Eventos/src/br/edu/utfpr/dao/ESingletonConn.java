package br.edu.utfpr.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * 
 * @author talyssoncastro
 */
public class ESingletonConn {
    
    private EntityManager entity;
    private static ESingletonConn singleton;
    
    private ESingletonConn()throws Exception{
        EntityManagerFactory fac = Persistence.createEntityManagerFactory("PU_EVENTOS");
        entity = fac.createEntityManager();
    }
    
    public static EntityManager getEntity() throws Exception {
        if (singleton == null){
            singleton = new ESingletonConn();
        }
        return singleton.entity;
    }
}
