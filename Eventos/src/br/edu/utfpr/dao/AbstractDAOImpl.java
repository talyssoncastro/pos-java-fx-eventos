/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.dao;

import br.edu.utfpr.model.AbstractEntity;
import javax.persistence.EntityManager;

/**
 * 
 * @author talyssoncastro
 * @param <T> 
 */
public abstract class AbstractDAOImpl<T extends AbstractEntity> implements AbstractDAO<T>{

    @Override
    public EntityManager getEntity() throws Exception {
        return ESingletonConn.getEntity();
    }
    
    @Override
    public T salvar(T obj) throws Exception {
        if (obj.isPersistente()){
            return alterar(obj);
        } else {
            return inserir(obj);
        }
    }

    @Override
    public T inserir(T obj) throws Exception {
        getEntity().persist(obj);
        getEntity().flush();
        return obj;
    }

    @Override
    public T alterar(T obj) throws Exception {
        getEntity().merge(obj);
        getEntity().flush();
        return obj;
    }

    @Override
    public Boolean excluir(Long id) throws Exception {
        T obj = getById(id);
        getEntity().remove(obj);
        getEntity().flush();
        return true;
    }

    @Override
    public Boolean excluir(T obj) throws Exception {
        obj = getById(obj.getId());
        getEntity().remove(obj);
        getEntity().flush();
        return true;
    }
    
}
