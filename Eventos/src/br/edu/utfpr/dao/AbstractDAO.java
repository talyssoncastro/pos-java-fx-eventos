/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.dao;

import br.edu.utfpr.model.AbstractEntity;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * 
 * @author talyssoncastro
 * @param <T> 
 */
public interface AbstractDAO<T extends AbstractEntity>  {
    
    public EntityManager getEntity() throws Exception;
    
    public T salvar(T obj)throws Exception;
    
    public T inserir(T obj)throws Exception;
    
    public T alterar(T obj)throws Exception;
    
    public Boolean excluir(Long id)throws Exception;
    
    public Boolean excluir(T obj)throws Exception;
    
    public T getById(Long id)throws Exception;
    
    public List<T> getTodos()throws Exception;
    
    
}
