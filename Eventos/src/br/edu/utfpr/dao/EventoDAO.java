/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.dao;

import br.edu.utfpr.model.Evento;
import java.util.List;
import javax.persistence.Query;

/**
 * 
 * @author talyssoncastro
 */
public class EventoDAO extends AbstractDAOImpl<Evento>{

    @Override
    public Evento getById(Long id) throws Exception {
        Query query = getEntity().createQuery("Select e From Evento e where e.idEvento = :id ");
        query.setParameter("id", id);
        return (Evento)query.getSingleResult();
    }

    @Override
    public List<Evento> getTodos() throws Exception {
        Query query = getEntity().createQuery("Select e From Evento e ");
        return query.getResultList();
    }
    
    public List<Evento> getByDescricao(String descricao) throws Exception {
        Query query = getEntity().createQuery("select e From Evento e where upper(e.descricao) like :descricao ");
        query.setParameter("descricao", "%"+descricao.toUpperCase().trim()+"%");
        return query.getResultList();
    }
            
}
