/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.controller;

import br.edu.utfpr.dao.ESingletonConn;
import br.edu.utfpr.facade.EventoFacade;
import br.edu.utfpr.model.Evento;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.persistence.EntityManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 * 
 * @author talyssoncastro
 */
public class PesquisaEventoCtrl implements Initializable {

    @FXML
    private Button btnIncluir;
    @FXML
    private Button btnExcluir;
    @FXML
    private Button btnAlterar;
    @FXML
    private Button btnImprimir;
    @FXML
    private TableView<Evento> table;
    @FXML
    private TextField txtPesquisa;
    private ObservableList<Evento> dataModel;// = new ArrayList<>();
    private EventoFacade eventoFacade = new EventoFacade();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            List<Evento> eventos = eventoFacade.getTodos();
            dataModel = FXCollections.observableList(eventos);
            table.setItems(dataModel);
        } catch (Exception ex) {
            ex.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Atenção");
            alert.setHeaderText("Falha na busca dos eventos para listagem!");
            alert.setContentText(ex.getLocalizedMessage());
            alert.show();
        }

    }

    @FXML
    public void onClickBtnIncluir(ActionEvent event) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/br/edu/utfpr/view/CadastroEvento.fxml"));
        Pane root = loader.load();
        Scene cena = new Scene(root);

        Stage stage = new Stage();
        stage.setIconified(false);
        stage.setScene(cena);
        stage.setTitle("Cadastro de Eventos");
        stage.setResizable(true);
        stage.centerOnScreen();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();
        CadastroEventoCtrl ctrl = loader.getController();
        if (ctrl.getOp() == ctrl.SALVAR) {
            Evento evento = ctrl.getEvento();
            evento = eventoFacade.salvar(evento);
            dataModel.add(evento);
        }
    }

    @FXML
    public void onClickBtnAlterar(ActionEvent event) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/br/edu/utfpr/view/CadastroEvento.fxml"));
        Pane root = loader.load();
        Scene cena = new Scene(root);
        if (!table.selectionModelProperty().isNull().get()) {
            Stage stage = new Stage();
            stage.setIconified(false);
            stage.setScene(cena);
            stage.setTitle("Cadastro de Evento");
            stage.setResizable(true);
            stage.centerOnScreen();
            stage.initModality(Modality.APPLICATION_MODAL);
            CadastroEventoCtrl ctrl = loader.getController();
            ctrl.setEvento(table.getSelectionModel().getSelectedItem());
            stage.showAndWait();

            if (ctrl.getOp() == ctrl.SALVAR) {
                Evento evento = ctrl.getEvento();
                int index = dataModel.indexOf(evento);
                evento = eventoFacade.salvar(evento);
                dataModel.set(index, evento);
            }
        }
    }

    @FXML
    public void onClickBtnExcluir(ActionEvent event) {
        if (!table.selectionModelProperty().isNull().get()) {
            Evento evento = table.getSelectionModel().getSelectedItem();
            try {
                if (eventoFacade.excluir(evento)) {
                    dataModel.remove(evento);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Atenção");
                alert.setHeaderText("Falha ao excluir o Evento.\nVerifique o conteúdo do erro!");
                alert.setContentText(ex.getLocalizedMessage());
                alert.showAndWait();
            }
        }
    }

    @FXML
    public void onClickBtnImprimir(ActionEvent event) {
        try {
            InputStream input = getClass().getResourceAsStream("/br/edu/utfpr/report/Eventos.jasper");
            JasperReport report = (JasperReport) JRLoader.loadObject(input);
            Map<String, Object> parametros = new HashMap<String, Object>();

            EntityManager entity = ESingletonConn.getEntity();
            entity.getTransaction().begin();
            Connection conn = entity.unwrap(Connection.class);
            entity.getTransaction().commit();
            JasperPrint print = null;
            print = JasperFillManager.fillReport(report, parametros, conn);
            JasperViewer viewer = new JasperViewer(print, false);
            viewer.setVisible(true);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    public void onClickBtnPesquisar(ActionEvent event) {
        try {
            List<Evento> eventos = eventoFacade.getByDescricao(txtPesquisa.getText());
            dataModel.clear();
            dataModel.addAll(FXCollections.observableList(eventos));
        } catch (Exception ex) {
            ex.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Atenção");
            alert.setHeaderText("Falha ao pesquisar pelo Modelo!");
            alert.setContentText(ex.getLocalizedMessage());
            alert.show();
        }
    }

    @FXML
    public void onKeyPressedTxtPesquisa(KeyEvent event) {
        if (event.getCode().equals(KeyCode.ENTER)) {
            onClickBtnPesquisar(null);
        }
    }
}
