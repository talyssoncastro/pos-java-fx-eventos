/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.controller;

import br.edu.utfpr.model.Evento;
import br.edu.utfpr.model.TipoTraje;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * 
 * @author talyssoncastro
 */
public class CadastroEventoCtrl implements Initializable {

    private Evento evento = new Evento();
    public final int CANCELAR = 1;
    public final int SALVAR = 2;
    private Integer op = CANCELAR;
    @FXML
    private TextField txtCodigo;
    @FXML
    private TextField txtDescricao;
    @FXML
    private DatePicker dtpData;
    @FXML
    private TextField txtValor;
    @FXML
    private TextField txtLocal;
    @FXML
    private ComboBox<TipoTraje> cbxTraje;
    @FXML
    private CheckBox ckbMeiaEntrada;
    @FXML
    private CheckBox ckbPermiteMenores;
    @FXML
    private Button btnCancelar;
    @FXML
    private Button btnSalvar;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        setEvento(evento);
        
        btnSalvar.disableProperty().bind(
                txtDescricao.textProperty().isEmpty().or(
                txtLocal.textProperty().isEmpty()).or(
                txtValor.textProperty().isEmpty()).or(
                dtpData.valueProperty().isNull()).or(
                cbxTraje.valueProperty().isNull())
        );
        
        List<TipoTraje> trajes = Arrays.asList(TipoTraje.values());
        cbxTraje.setItems(FXCollections.observableList(trajes));
        
    }

    @FXML
    public void btnCancelarClick(ActionEvent event) {
        op = CANCELAR;
        Stage stage = (Stage) btnCancelar.getScene().getWindow();
        stage.close();
    }

    @FXML
    public void btnSalvarClick(ActionEvent event) {
        op = SALVAR;
        Stage stage = (Stage) btnCancelar.getScene().getWindow();
        stage.close();
    }

    public Evento getEvento() {
        return evento;
    }

    public void setEvento(Evento evento) {
        this.evento = evento;
        txtCodigo.textProperty().bind(Bindings.format("%d", evento.getId()));
        txtDescricao.textProperty().bindBidirectional(evento.descricaoProperty());
        txtValor.textProperty().bindBidirectional(evento.valorProperty(), new DecimalFormat());
        dtpData.valueProperty().bindBidirectional(evento.dataProperty());
        txtLocal.textProperty().bindBidirectional(evento.localProperty());
        cbxTraje.valueProperty().bindBidirectional(evento.tipoTrajeProperty());
        ckbMeiaEntrada.selectedProperty().bindBidirectional(evento.possuiMeiaEntradaProperty());
        ckbPermiteMenores.selectedProperty().bindBidirectional(evento.permiteMenoresProperty());
    }

    public Integer getOp() {
        return op;
    }

}
