/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Anderson
 */
public class FXMLController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private Pane pnlDesktop;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    @FXML
    public void onExit(){
        Platform.exit();
    }
    
    public void addEventOnEsc(Scene scena){
        scena.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>(){
            @Override
            public void handle(KeyEvent event) {
                Stage st = (Stage)scena.getWindow();
                st.close();
            }
        });
    }
    
    @FXML
    public void abrirEvento() throws Exception {
        Pane root = FXMLLoader.load(getClass().getResource("/br/edu/utfpr/view/PesquisaEvento.fxml"));
        Scene cena = new Scene(root);
        addEventOnEsc(cena);
        Stage stage = new Stage();
        stage.setIconified(false);
        stage.setScene(cena);
        stage.setTitle("Pesquisa de Eventos");
        stage.setResizable(true);
        stage.initOwner(pnlDesktop.getScene().getWindow());
        stage.centerOnScreen();
        stage.show();        
    }

}
