package br.edu.utfpr;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * 
 * @author talyssoncastro
 */
public class Main extends Application {
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        String resource = "/br/edu/utfpr/view/FXML.fxml";
        Pane root = FXMLLoader.load(getClass().getResource(resource));
        Scene scene = new Scene(root);
        primaryStage.setTitle("Formulario");
        primaryStage.setScene(scene);
//        primaryStage.sizeToScene();        
        primaryStage.show();
        primaryStage.setMaximized(true);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
