package br.edu.utfpr.facade;

import br.edu.utfpr.model.AbstractEntity;
import java.util.List;

/**
 * 
 * @author talyssoncastro
 * @param <T> 
 */
public abstract class AbstractFacadeImpl<T extends AbstractEntity> implements  AbstractFacade<T>{

    @Override
    public T salvar(T obj) throws Exception {
        try {
            begin();
            obj = getDAO().salvar(obj);
            commit();
            return obj;
        }catch (Exception ex){
            ex.printStackTrace();
            rollback();
            throw ex;
        }
    }

    @Override
    public Boolean excluir(T obj) throws Exception {
        try {
            begin();
            Boolean ret = getDAO().excluir(obj);
            commit();
            return ret;
        }catch (Exception ex){
            rollback();
            throw ex;
        }
    }

    @Override
    public Boolean excluir(Long id) throws Exception {
        try {
            begin();
            Boolean ret = getDAO().excluir(id);
            commit();
            return ret;
        }catch (Exception ex){
            rollback();
            throw ex;
        }
    }

    @Override
    public T getById(Long id) throws Exception {
        return getDAO().getById(id);
    }

    @Override
    public List<T> getTodos() throws Exception {
        return getDAO().getTodos();
    }

    @Override
    public void begin() throws Exception {
        getDAO().getEntity().getTransaction().begin();
    }

    @Override
    public void rollback() throws Exception {
        getDAO().getEntity().getTransaction().rollback();
    }

    @Override
    public void commit() throws Exception {
        getDAO().getEntity().getTransaction().commit();
    }
    
}
