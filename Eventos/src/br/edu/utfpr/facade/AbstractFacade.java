/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.facade;

import br.edu.utfpr.dao.AbstractDAO;
import br.edu.utfpr.model.AbstractEntity;
import java.util.List;

/**
 * 
 * @author talyssoncastro
 * @param <T> 
 */
public interface AbstractFacade<T extends AbstractEntity>{
    
    public T salvar(T obj) throws Exception;
    
    public Boolean excluir(T obj) throws Exception;
    
    public Boolean excluir(Long id) throws Exception;
    
    public T getById(Long id) throws Exception;
    
    public List<T> getTodos() throws Exception;
            
    public AbstractDAO<T> getDAO()throws Exception;
    
    public void begin()throws Exception;
    
    public void rollback()throws Exception;
    
    public void commit()throws Exception;
}
