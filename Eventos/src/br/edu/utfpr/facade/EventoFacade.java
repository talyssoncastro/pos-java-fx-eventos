package br.edu.utfpr.facade;

import br.edu.utfpr.dao.AbstractDAO;
import br.edu.utfpr.dao.EventoDAO;
import br.edu.utfpr.model.Evento;
import java.util.List;

/**
 * 
 * @author talyssoncastro
 */
public class EventoFacade extends AbstractFacadeImpl<Evento> {

    private EventoDAO eventoDAO;

    public EventoFacade() {
        eventoDAO = new EventoDAO();
    }

    @Override
    public AbstractDAO<Evento> getDAO() throws Exception {
        return eventoDAO;
    }

    public List<Evento> getByDescricao(String descricao) throws Exception {
        return eventoDAO.getByDescricao(descricao);
    }
}
